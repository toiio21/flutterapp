import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
void main() => runApp(HomePage());
  

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var now = new DateTime.now();
  List data;
  String _search = 'nature';
  @override
  void initState() {
    super.initState();
    this.getjsondata();
  }

  Future<String> getjsondata() async {
    try {
      var response = await http.get('https://api.unsplash.com/search/photos?per_page=30&client_id=X-65o2sDYE_e3tIj0g9QJF3F00ZNXw1hMyekY0504LI&query=$_search');
      setState(() {
        var converted = json.decode(response.body);
        data = converted['results'];
      });
    } catch(e){return e;}
  }
  Icon cusicon = Icon(Icons.search);
  Widget custext = Text('Wallpaper');
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wallpaper', 
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: (){},
            tooltip: 'menu', 
            icon: Icon(
              Icons.menu),
          ),
          title: custext,
          actions: <Widget>[IconButton(onPressed: (){setState(() {
            if(this.cusicon.icon == Icons.search) {
              this.cusicon = Icon(Icons.cancel);
              this.custext = TextField(autofocus: true, onChanged: (text){setState(() {
                _search = text;
              });
              getjsondata();
              },
              textInputAction: TextInputAction.done, 
              decoration: InputDecoration(
                border: InputBorder.none, 
                hintText: 'Search Wallpaper', 
                hintStyle: TextStyle(
                  color: Colors.white,)
              ),
              style: TextStyle(color: Colors.white, fontSize: 20),);}
            else{
              this.cusicon = Icon(Icons.search);
              this.custext = Text('Wallpapers');
            }
          });
          },
          icon: cusicon,)
          ],
        ),
        body: new ListView.builder(
          itemCount: data == null ?0: data.length, 
          itemBuilder: (
            BuildContext context, int index){
              return Container(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch, 
                    children: <Widget>[new Card(
                      child : new Container(
                        padding: EdgeInsets.all(20), 
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start, 
                          children: <Widget>[
                            Hero(
                              tag: 1, 
                              child: Image.
                                network(data[index]['urls']['small'], 
                                width: MediaQuery.of(context).size.width,),
                                )
                            
                            
                          ],
                        ),
                      )
                    )
                  ],
                  ),
                ),
              );},
          ),
      )
    );
    
  }
}